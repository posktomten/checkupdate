QT -= gui
QT += core network
CONFIG += c++11 console
CONFIG -= app_bundle

TARGET = checkupdate


# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
#DEFINES += QT_DEPRECATED_WARNINGS
DEFINES += QT_DISABLE_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0
#CONFIG += /opt/Qt5.14.1/5.14.1/gcc_64/bin//lrelease embed_translations

HEADERS += \
    main.h \
    dostuff.h


SOURCES += \
        dostuff.cpp \
        main.cpp \
        license.cpp \
        readfile.cpp

RESOURCES = resurs.qrc

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

DESTDIR=../build-executable
