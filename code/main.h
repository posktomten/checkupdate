
#include <QCoreApplication>
#include <QCommandLineParser>
#include <QDebug>
#include <QFile>
#include <QTextStream>


#include "dostuff.h"




#ifdef __clang__
#define COMPILER2 "clang"
#endif
#define DEFALT_DELAY 1000
#define BUILD_DATE_TIME "The source code was changed: " __TIMESTAMP__"\nThe source code was compiled: " __DATE__ " " __TIME__

#define APPLICATION_DESCRIPTION "\ncheckupdate 1.0.0 Copyright (C) 2020 Ingemar Ceicer.\nThe source code of the program can be found here:\nhttps://gitlab.com/posktomten/checkupdate\nThis program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License version 2 aspublished by the Free Software Foundation. This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details."




//#define VP1 "http://bin.ceicer.com/hash/version2.txt"
//#define VP2 "http://bin.ceicer.com/streamcapture2/version2.txt"

