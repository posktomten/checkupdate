
//#define VP1 "http://bin.ceicer.com/hash/version2.txt"
//#define VP2 "http://bin.ceicer.com/streamcapture2/version2.txt"
///
#ifndef MAIN_H
#define MAIN_H

#include "main.h"




bool license_short();
bool license();
QStringList readfile(const QString &filename);
int main(int argc, char *argv[])
{
    auto *app = new QCoreApplication(argc, argv);
    QStringList argument = QCoreApplication::arguments();
    QCoreApplication::setApplicationVersion("1.0.0");
    QTextStream cout(stdout);
    QString COMPILER = "";
#ifdef __GNUC__
#ifdef __MINGW32__
    COMPILER = "MinGW GCC " + QString::number(__GNUC__) + "." + QString::number(__GNUC_MINOR__) + "." + QString::number(__GNUC_PATCHLEVEL__);
#else
    COMPILER = "GCC " + QString::number(__GNUC__) + "." + QString::number(__GNUC_MINOR__) + "." + QString::number(__GNUC_PATCHLEVEL__);
#endif
#endif
#ifdef __ICC
    COMPILER =  "Intel icc version: " + QString::number(__INTEL_COMPILER) + " The compiler build date: " + QString::number(__INTEL_COMPILER_BUILD_DATE) + " minor update number: " + QString::number(__INTEL_COMPILER_UPDATE);
#endif
#ifdef __clang__
    COMPILER =  "clang version: " + QString::fromStdString(__clang_version__);
#endif
//    QTextStream cin(stdin);
    QCommandLineParser parser;
    parser.addVersionOption();
    //  parser.setApplicationDescription(APPLICATION_DESCRIPTION);
    parser.setApplicationDescription(APPLICATION_DESCRIPTION);
    parser.addHelpOption();
    parser.addOption({{"a", "address"}, "Space separated list of addresses for version information <https://...>.", "https://..."});
    parser.addOption({{"f", "file"}, "File with addresses, separated by newline.", "file name"});
    parser.addOption({{"t", "time"}, "The delay before the program ends, in ms. Defalt 1000 ms", "ms"});
    parser.addOption({{"l", "license"}, "View the program's license", ""});
    parser.addOption({{"d", "date"}, "View build date and time", ""});
    parser.addOption({{"c", "compiler"}, "View compiler", ""});
    parser.process(*app);
    QStringList argumentlista = QCoreApplication::arguments();
    argumentlista.removeFirst(); // Remove program name

    if(argumentlista.contains("-l") || argumentlista.contains("--license")) {
        license();
        return 0;
    }

    if(argumentlista.contains("-c") || argumentlista.contains("--compiler")) {
        cout << COMPILER << endl;
        return 0;
    }

    int tid = DEFALT_DELAY;
    QString stid;

    if(argumentlista.contains("-t")) {
        int index = argumentlista.indexOf("-t");
        stid = argumentlista.at(index + 1);
        bool ok;
        tid = stid.toInt(&ok);

        if(!ok)
            tid = DEFALT_DELAY;
    }

    if(argumentlista.contains("--time")) {
        int index = argumentlista.indexOf("--time");
        stid = argumentlista.at(index + 1);
        bool ok;
        tid = stid.toInt(&ok);

        if(!ok)
            tid = DEFALT_DELAY;
    }

    if(argumentlista.contains("-f")) {
        int index = argumentlista.indexOf("-f");
        argumentlista = readfile(argumentlista.at(index + 1));
    }

    if(argumentlista.contains("--file")) {
        int index = argumentlista.indexOf("--file");
        argumentlista = readfile(argumentlista.at(index + 1));
    }

    if(argumentlista.contains("-d") || argumentlista.contains("--date")) {
        cout << BUILD_DATE_TIME << endl;
        return 0;
    }

    for(int i = 0; i < argumentlista.size(); i++) {
        if((argumentlista.at(i) == "-t") || (argumentlista.at(i) == "--time")) {
            argumentlista.removeAt(i + 1);
        }
    }

    QStringList bort = {"-a", "--address", "-t", "--time", "-d", "--date", "-f", "--file"};

    for(const QString &s : bort) {
        if(argumentlista.contains(s)) {
            argumentlista.removeAll(s);
        }
    }

    //  DoStuff *mDoStuff = new DoStuff;
    DoStuff mDoStuff;
//    QObject::connect(mDoStuff, &DoStuff::finished, app, &QCoreApplication::quit, Qt::QueuedConnection);
    QObject::connect(&mDoStuff, &DoStuff::finished, app, &QCoreApplication::quit);
    const QStringList &versionPath = argumentlista;
    mDoStuff.run(versionPath, tid);
    return QCoreApplication::exec();
    // return 0;
}

#endif //MAIN_H

