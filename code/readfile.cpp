#ifndef MAIN_H
#define MAIN_H
#include "main.h"


QStringList readfile(const QString &filename)
{
    QTextStream cout(stdout);
    QStringList argumentlista;
    QString s = "";
    QFile file(filename);

    if(!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        argumentlista.clear();
        cout << "File not found" << endl << endl;
        return argumentlista;
    }

    QString tmp;

    while(!file.atEnd()) {
        QByteArray line = file.readLine();
        tmp = QString(line);

        if(tmp.at(0) != "#")
            s = s + tmp;
    }

    argumentlista = s.split("\n", QString::SkipEmptyParts);
    return argumentlista;
}

#endif //MAIN_H
