#include "dostuff.h"
bool checkline(const QString &line);
void DoStuff::run(const QStringList &argumentlista,  int &tid)
{
    if(argumentlista.isEmpty()) {
        tid = 0;
    }

    foreach(QString s, argumentlista) {
        QUrl url(s);
        auto *nam = new QNetworkAccessManager(this);
        auto *reply = nam->get(QNetworkRequest(url));
        QObject::connect(reply, &QNetworkReply::finished, [ reply, s ]()  {
            QByteArray bytes = reply->readAll();  // bytes
            QString info(bytes); // string
            QTextStream cout(stdout);

            if(info.size() < 1) {
                cout << endl << "\"" << s << "\"" << endl;
                cout << "Incorrect address or network connection error" << endl;
                // return;
            }

            info = info.trimmed();
            int index = info.indexOf("\n");

            if(index < 0)
                index = info.size();

            QString tmp = info.left(index);

            if(checkline(tmp)) {
                cout << s << endl;
                cout << info << endl << endl;
                return;
            }

            cout << s << endl;
            cout << "No version information can be found." << endl << endl;
        });
    }

    auto *timer = new QTimer(nullptr);
    connect(timer, &QTimer::timeout, [this]()->void{
        emit finished();
    });
    timer->start(tid);
}


bool checkline(const QString &line)
{
    QRegularExpression qreg(R"(^(?:(\d+)\.)?(?:(\d+)\.)?(\*|\d+)$)");
    QRegularExpressionMatch match = qreg.match(line);
    return  match.hasMatch();
}
