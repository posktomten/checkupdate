#ifndef MAIN_H
#define MAIN_H
#include "main.h"

bool license_short()
{
    QFile file(":/txt/license_short.txt");
    QTextStream in(&file);

    if(!file.open(QFile::ReadOnly | QFile::Text))
        return false;

    QTextStream cout(stdout);
    QString s = in.readAll();
    cout << s << endl;
    return true;
}
bool license()
{
    QFile file(":/txt/license.txt");
    QTextStream in(&file);

    if(!file.open(QFile::ReadOnly | QFile::Text))
        return false;

    QTextStream cout(stdout);
    QString s = in.readAll();
    cout << s << endl;
    return true;
}
#endif //MAIN_H
