#ifndef DOSTUFF_H
#define DOSTUFF_H
#include <QObject>
#include <QNetworkReply>
#include <QTextStream>
#include <QTimer>
#include <QDebug>
#include <QRegularExpression>
class DoStuff: public QObject
{
    Q_OBJECT
public:
    DoStuff(QObject *parent = nullptr) : QObject(parent) {}
    ~DoStuff() {}

public slots:
    void run(const QStringList &argumentlista, int &tid);



signals:
    void finished();
};

#endif // DOSTUFF_H
